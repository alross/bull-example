"use strict";

// Get router and controller
const queueRouter = require("express").Router();
const queueController = require("../controller/queueController");

// Perform GET on users
queueRouter.get("/", (req, res) => {
    queueController.processQueues();
});
module.exports = queueRouter;
