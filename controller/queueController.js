var Queue = require("bull");
const { createQueues } = require("bull-board");

const redisConfig = {
  redis: {
    port: process.env.REDIS_PORT,
    host: process.env.REDIS_HOST,
    password: process.env.REDIS_PASSWORD
  }
};

const queues = createQueues(redisConfig);

const helloQueue = queues.add("helloQueue"); // adds a queue

// defines how the queue works
helloQueue.process(async job => {
  console.log(`Hello ${job.data.hello}`);
});

helloQueue.add({ hello: "world" }); // adds a job to the queue

module.exports.processQueues = function processQueues() {
  console.log("Processing queues...");
};
