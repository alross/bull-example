"use strict";

// express app dependencies
const express = require("express");
const app = express();
const { UI } = require("bull-board");

const PORT = 5000; // using port 5000

// declare routers
const queueRouter = require("./routes/queueRoute");

app.use("/queues", queueRouter);
app.use("/admin/queues", UI);

// listen on specified port
app.listen(PORT, () =>
  console.log("Welcome to my User service on port: " + PORT)
);
